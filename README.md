# Table of contents

- [Table of contents](#table-of-contents)
- [Contributing](#contributing)
  - [Requirements](#requirements)
  - [Installing Nix](#installing-nix)
  - [Installing makes](#installing-makes)
  - [Git repository](#git-repository)
  - [Git configuration](#git-configuration)
  - [Commits structure and Merge Request policy](#commits-structure-and-merge-request-policy)
    - [Commits syntax](#commits-syntax)
      - [Rules](#rules)
      - [Example of compliant commit](#example-of-compliant-commit)
      - [Possible combinations](#possible-combinations)
      - [Recommendations](#recommendations)
      - [Another example](#another-example)

<!-- http://ecotrust-canada.github.io/markdown-toc -->

# Contributing

## Requirements

The instructions presented below will work correctly in any
debian/alpine based x86_64-linux operating system with fabric configurations
in user-space and DNS set to 1.1.1.1, 8.8.8.8, and 8.8.4.4.

Additionally you'll require sudo access to root.

## Installing Nix

```bash
curl -sL https://nixos.org/nix/install | sh
```

## Installing makes

Install makes by running the following command
```bash
nix-env -if https://github.com/fluidattacks/makes/archive/22.08.tar.gz
```

If everything goes well, you should be able to run
```bash
m
```

## Git repository

1. Follow [these](https://gitlab.com/help/ssh/README#generating-a-new-ssh-key-pair)
   instructions to generate your SSH key pair
1. Follow [these](https://docs.gitlab.com/ee/user/project/repository/gpg_signed_commits/)
   instructions to generate a GPG key pair
1. Clone the repository into any folder

    ```bash
    $ git clone git@gitlab.com:zettaploomorg/testing-makes.git
    Cloning into 'testing-makes'...
    # ...
    ```

## Git configuration

1. Tell Git who you are:

    ```bash
    $ git config --global user.name "Benito Martinez"
    $ git config --global user.email bmartinez@email.com

    # These settings are useful, too
    $ git config --global commit.gpgsign true
    ```
2. Check if the changes were successfully registered with:

    ```bash
    $ git config --list
    commit.gpgsign=true
    user.email=bmartinez@fluidattacks.com
    user.name=Benito Martinez
    ```

## Commits structure and Merge Request policy

You should create a branch with the username you use in your GitLab account. **Only one commit per Merge Request (MR) is allowed**, MRs with more than one commit will be closed.

### Commits syntax

Valid commit messages
have the structure:

```markup
[product]\[type]([scope]): #[issue-number]{.issue-part} [title] // This is the commit title
               // This blank line separates the commit title from the commit body
[body]         // This is the commit body. It CAN have multiple lines
```

- **[variable]** are **required** variables
    that must be replaced
    in a final commit message
    (**[]** symbols must be removed)
- **{variable}** are **optional** variables
    that must be replaced
    or removed
    in a final commit message
    (**{}** symbols must be removed)
- **// Comment** are comments
    that must be removed
    in a final commit message

#### Rules

Our product is called **lol**, all the other variables will be described bellow:

1. **[type]** variable has to be
   one of the following:

    ```markup
    rever  // Revert to a previous commit in history
    feat   // New feature
    perf   // Improves performance
    fix    // Bug fix
    refac  // Neither fixes a bug or adds a feature
    test   // Adding missing tests or correcting existing tests
    style  // Do not affect the meaning of the code (formatting, etc)
    ```

2. **[scope]** variable has to be
   one of the following:

    ```markup
    front  // Front-End change
    back   // Back-End change
    infra  // Infrastructure change
    conf   // Configuration files change
    build  // Build system, CI, compilers, etc (scons, webpack...)
    job    // asynchronous or schedule tasks (backups, maintenance...)
    cross  // Mix of two or more scopes
    doc    // Documentation only changes
    ```

3. **[product]** variable has to be
   one of the following:

    ```markup
    forces // Changes in forces
    integrates // Changes in Integrates
    all // Changes that affect both integrates and forces
    ```

4. A **Commit title**
   must exist.

5. A **Commit title**
   must **not** contain
   the '**:**' character.

6. **Commit title**
   must have 60 characters
   or less.

7. **Commit title**
   must be lower case.

8. **Commit title**
   must not finish
   with a dot '**.**'.

9. **Commit title**
   must reference
   an issue.

10. **Commit title**
   must be meaningful.
   Avoid using things like
   `feat(build)[integrates]: #5.1 feature`.

11. A **blank line**
    between commit title
    and commit body
    must exist.

12. A **commit body**
    must exist.

13. Lines in **commit body**
    must have 72 characters
    or less.

#### Example of compliant commit
```markup
lol\feat(front): #2 implemented front login

- implemented login in cognito
- used s3 to host the static files
```

#### Possible combinations

Below is a table explaining
all the possible combinations
between types and scopes
for a commit message
(Types are columns, scopes are rows):

|           |                  **rever**                  |                           **feat**                           |               **perf**               |                **fix**                |                **refac**                 |              **test**              |                 **style**                 |
| :-------: | :-----------------------------------------: | :----------------------------------------------------------: | :----------------------------------: | :-----------------------------------: | :--------------------------------------: | :--------------------------------: | :---------------------------------------: |
| **front** |   Revert front-end to a previous version    |                 Add new feature to front-end                 |      Improve perf in front-end       |      Fix something in front-end       |      Change something in front-end       |      Add tests for front-end       |        Change front-end code style        |
| **back**  |    Revert back-end to a previous version    |                 Add new feature to back-end                  |       Improve perf in back-end       |       Fix something in back-end       |       Change something in back-end       |       Add tests for back-end       |        Change back-end code style         |
| **infra** |     Revert infra to a previous version      |                   Add new feature to infra                   |        Improve perf in infra         |        Fix something in infra         |        Change something in infra         |        Add tests for infra         |          Change infra code style          |
| **conf**  |  Revert config files to previous a version  |               Add new feature to config files                |                  NA                  |     Fix something in config files     |     Change something in config files     |                 NA                 |      Change config files code style       |
| **build** | Revert building tools to previous a version | Add new feature to building tools or add a new building tool |        Improve building perf         |    Fix something in building tools    |    Change something in building tools    |    Add tests for building tools    |     Change building tools code style      |
|  **job**  |      Revert jobs to previous a version      |           Add new feature to jobs or add a new job           |          Improve jobs perf           |         Fix something in jobs         |         Change something in jobs         |         Add tests for jobs         |          Change jobs code style           |
| **cross** | Revert several scopes to previous a version |              Add new feature for several scopes              | Improve perf in several system parts | Fix something in several system parts | Change something in several system parts | Add tests for several system parts | Change code style in several system parts |
|  **doc**  |      Revert doc to a previous version       |                         Add new doc                          |                  NA                  |         Fix something in doc          |         Change something in doc          |                 NA                 |             Change doc style              |

Where:

- **perf** is performance.
- **infra** is infrastructure.
- **config** is configuration.
- **doc** is documentation.
- **NA** is not applicable.

#### Recommendations

- Try to itemize your commit body:

    ```
    - Add feature X in file Y
    - Run script Z
    - Remove file A with B purpose
    ```

#### Another example

Here is an example
of a compliant commit message
(Notice how the issue has
a '**.1**' right after,
meaning that such commit
is the part 1 for solving
the issue):

```markup
integrates\feat(build): #13.1 add type_check

- Add type_check function
- Remove unnecessary print_output function
```

<!-- ## Local back-end server

Run each command in a different terminal:

```bash
m . /integrates/back
m . /integrates/cache
m . /integrates/db
m . /integrates/storage
```

## Local web application

Run each command in a different terminal:

```bash
m . /integrates/front
``` -->
