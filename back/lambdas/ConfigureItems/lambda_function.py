import json
import os
import pymysql


def lambda_handler(event, context):
    # TODO implement

    conn = pymysql.connect(
        host=os.environ["database_host"],
        user=os.environ["database_user"],
        passwd=os.environ["database_password"],
        db=os.environ["database_name"],
    )

    cur = conn.cursor()
    error = ""

    if event["httpMethod"] == "GET":

        buildItems = []
        items = {}
        request = event["queryStringParameters"]

        if "user" in request:
            user = request["user"]

            if not cur.execute("SELECT * FROM User where id = %s", str(user)):
                error = "No se ha encontrado el usuario asociado"
                return {
                    "statusCode": 404,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Origin": "*",
                        "Allow": "GET, OPTIONS, POST",
                        "Access-Control-Allow-Methods": "GET, OPTIONS, POST",
                        "Access-Control-Allow-Headers": "*",
                    },
                    "body": json.dumps({"detail": error}),
                }

            if "champion" in request:

                champion = request["champion"]
                index = []
                index.append("user")

                if not cur.execute(
                    "SELECT * FROM User_Choosing_Items where user = %s and champion = %s",
                    (str(user), str(champion)),
                ):
                    error = "No se ha detectado que el usuario tenga alguna configuracion de items"
                    return {
                        "statusCode": 404,
                        "headers": {
                            "Content-Type": "application/json",
                            "Access-Control-Allow-Origin": "*",
                            "Allow": "GET, OPTIONS, POST",
                            "Access-Control-Allow-Methods": "GET, OPTIONS, POST",
                            "Access-Control-Allow-Headers": "*",
                        },
                        "body": json.dumps({"detail": error}),
                    }

                consulta = cur.fetchall()

                buildItems.append(consulta[0][1])

                for item in consulta:
                    items = {
                        "id": item[0],
                        "items": [item[i] for i in range(3, 9)],
                    }
                    buildItems.append(items)

                    index.append(item[2])

                items = {i: info for i, info in zip(index, buildItems)}

                return {
                    "statusCode": 200,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Origin": "*",
                        "Allow": "GET, OPTIONS, POST",
                        "Access-Control-Allow-Methods": "GET, OPTIONS, POST",
                        "Access-Control-Allow-Headers": "*",
                    },
                    "body": json.dumps(items),
                }
            else:
                error = "No se ha detectado un campeon"
                return {
                    "statusCode": 409,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Origin": "*",
                        "Allow": "GET, OPTIONS, POST",
                        "Access-Control-Allow-Methods": "GET, OPTIONS, POST",
                        "Access-Control-Allow-Headers": "*",
                    },
                    "body": json.dumps({"detail": error}),
                }
        else:
            error = "No se ha detectado un usuario"
            return {
                "statusCode": 409,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": "*",
                    "Allow": "GET, OPTIONS, POST",
                    "Access-Control-Allow-Methods": "GET, OPTIONS, POST",
                    "Access-Control-Allow-Headers": "*",
                },
                "body": json.dumps({"detail": error}),
            }

    elif event["httpMethod"] == "POST":

        request_json = json.loads(event["body"])
        user = request_json["user"]
        champion = request_json["champion"]
        items = request_json["items"]

        if not cur.execute("SELECT * FROM User where id = %s", str(user)):
            error = "No se ha encontrado el usuario asociado"
            return {
                "statusCode": 404,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": "*",
                    "Allow": "GET, OPTIONS, POST",
                    "Access-Control-Allow-Methods": "GET, OPTIONS, POST",
                    "Access-Control-Allow-Headers": "*",
                },
                "body": json.dumps({"detail": error}),
            }

        if len(items) == 6:
            for i in range(0, len(items)):
                for j in range(i + 1, len(items)):
                    error = "The user it is choosing the same Spell."
                    if items[i].__eq__(items[j]):
                        return {
                            "statusCode": 409,
                            "headers": {
                                "Content-Type": "application/json",
                                "Access-Control-Allow-Origin": "*",
                                "Allow": "GET, OPTIONS, POST",
                                "Access-Control-Allow-Methods": "GET, OPTIONS, POST",
                                "Access-Control-Allow-Headers": "*",
                            },
                            "body": json.dumps({"detail": error}),
                        }

            sql = "INSERT INTO User_Choosing_Items (`User`, Champion, Item_1, Item_2,Item_3, Item_4, Item_5, Item_6) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"
            cur.execute(
                sql,
                (
                    user,
                    champion,
                    items[0],
                    items[1],
                    items[2],
                    items[3],
                    items[4],
                    items[5],
                ),
            )
            conn.commit()

            return {
                "statusCode": 201,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": "*",
                    "Allow": "GET, OPTIONS, POST",
                    "Access-Control-Allow-Methods": "GET, OPTIONS, POST",
                    "Access-Control-Allow-Headers": "*",
                },
                "body": event["body"],
            }
        else:
            error = "No se ha enviado la cantidad correcta de parametros"
            return {
                "statusCode": 409,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": "*",
                    "Allow": "GET, OPTIONS, POST",
                    "Access-Control-Allow-Methods": "GET, OPTIONS, POST",
                    "Access-Control-Allow-Headers": "*",
                },
                "body": json.dumps({"detail": error}),
            }
