import json
import os
import pymysql


def lambda_handler(event, context):

    conn = pymysql.connect(
        host=os.environ["database_host"],
        user=os.environ["database_user"],
        passwd=os.environ["database_password"],
        db=os.environ["database_name"],
    )
    cur = conn.cursor()

    if event["httpMethod"] == "GET":

        request_json = event["queryStringParameters"]
        userid = request_json["user"]
        if "champion_skin" in request_json:
            skinName = request_json["champion_skin"]

            cur.execute(
                "SELECT Champion_skin, is_Champion FROM User_Shopping_Skins WHERE User = %s AND Champion_Skin = %s",
                (str(userid), str(skinName)),
            )
            cons = cur.fetchall()
            if not cons:
                error = "No existe el usuario o skin"
                return {
                    "statusCode": 404,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Origin": "*",
                        "Allow": "GET, OPTIONS, POST",
                        "Access-Control-Allow-Methods": "GET, OPTIONS, POST",
                        "Access-Control-Allow-Headers": "*",
                    },
                    "body": json.dumps({"detail": error}),
                }
            else:
                championSkin = cons[0][0]
                isChampion = cons[0][1]
                data = {
                    "msj": "ok",
                    "id": userid,
                    "champion_skin": championSkin,
                    "is_champion": isChampion,
                }
                return {
                    "statusCode": 200,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Origin": "*",
                        "Allow": "GET, OPTIONS, POST",
                        "Access-Control-Allow-Methods": "GET, OPTIONS, POST",
                        "Access-Control-Allow-Headers": "*",
                    },
                    "body": json.dumps(data),
                }
        else:
            cur.execute(
                "SELECT Champion_skin, is_Champion FROM User_Shopping_Skins WHERE User = %s",
                str(userid),
            )
            skins = cur.fetchall()
            result = []
            for skin in skins:
                result.append(
                    {"champion_skin": skin[0], "is_champion": skin[1]}
                )
            data = {"msj": "ok", "id": userid, "Skins": result}
            return {
                "statusCode": 200,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": "*",
                    "Allow": "GET, OPTIONS, POST",
                    "Access-Control-Allow-Methods": "GET, OPTIONS, POST",
                    "Access-Control-Allow-Headers": "*",
                },
                "body": json.dumps(data),
            }

    elif event["httpMethod"] == "POST":

        request_json = json.loads(event["body"])
        userid = request_json["userid"]
        championName = request_json["championName"]
        skinName = request_json["skinName"]
        price = request_json["price"]

        cur.execute("SELECT User_coins FROM User WHERE id = %s ", (userid))
        moneycheck = cur.fetchall()[0][0] - price

        cons = cur.execute(
            "SELECT Champion_Skin FROM User_Shopping_Skins WHERE User = %s AND Champion_Skin = %s",
            (str(userid), str(skinName)),
        )
        if not cons:
            if championName == skinName:
                if moneycheck >= 0:
                    cur.execute(
                        "UPDATE User SET User_coins= %s WHERE id = %s",
                        (str(moneycheck), str(userid)),
                    )
                    conn.commit()
                    sql = "INSERT INTO `User_Shopping_Skins` (`User`, `Champion_Skin`, `is_Champion`, `Skin_Price`) VALUES (%s, %s, %s, %s)"
                    cur.execute(
                        sql, (str(userid), str(skinName), str(1), str(price))
                    )
                    conn.commit()
                    data = {"msj": "ok", "coins": moneycheck}
                    return {
                        "statusCode": 200,
                        "headers": {
                            "Content-Type": "application/json",
                            "Access-Control-Allow-Origin": "*",
                            "Allow": "GET, OPTIONS, POST",
                            "Access-Control-Allow-Methods": "GET, OPTIONS, POST",
                            "Access-Control-Allow-Headers": "*",
                        },
                        "body": json.dumps(data),
                    }
                else:
                    error = "No tiene suficiente dinero"
                    return {
                        "statusCode": 404,
                        "headers": {
                            "Content-Type": "application/json",
                            "Access-Control-Allow-Origin": "*",
                            "Allow": "GET, OPTIONS, POST",
                            "Access-Control-Allow-Methods": "GET, OPTIONS, POST",
                            "Access-Control-Allow-Headers": "*",
                        },
                        "body": json.dumps({"detail": error}),
                    }
            else:
                cons = cur.execute(
                    "SELECT Champion_Skin FROM User_Shopping_Skins WHERE User = %s AND Champion_Skin = %s",
                    (str(userid), str(championName)),
                )
                if not cons:
                    error = "No tene el campeon"
                    return {
                        "statusCode": 404,
                        "headers": {
                            "Content-Type": "application/json",
                            "Access-Control-Allow-Origin": "*",
                            "Allow": "GET, OPTIONS, POST",
                            "Access-Control-Allow-Methods": "GET, OPTIONS, POST",
                            "Access-Control-Allow-Headers": "*",
                        },
                        "body": json.dumps({"detail": error}),
                    }
                else:
                    if moneycheck >= 0:
                        cur.execute(
                            "UPDATE User SET User_coins= %s WHERE id = %s",
                            (str(moneycheck), str(userid)),
                        )
                        conn.commit()
                        sql = "INSERT INTO `User_Shopping_Skins` (`User`, `Champion_Skin`, `Skin_Price`) VALUES (%s, %s, %s)"
                        cur.execute(
                            sql, (str(userid), str(skinName), str(price))
                        )
                        conn.commit()
                        data = {"msj": "ok", "coins": moneycheck}
                        return {
                            "statusCode": 200,
                            "headers": {
                                "Content-Type": "application/json",
                                "Access-Control-Allow-Origin": "*",
                                "Allow": "GET, OPTIONS, POST",
                                "Access-Control-Allow-Methods": "GET, OPTIONS, POST",
                                "Access-Control-Allow-Headers": "*",
                            },
                            "body": json.dumps(data),
                        }
                    else:
                        error = "No tiene suficiente dinero"
                        return {
                            "statusCode": 404,
                            "headers": {
                                "Content-Type": "application/json",
                                "Access-Control-Allow-Origin": "*",
                                "Allow": "GET, OPTIONS, POST",
                                "Access-Control-Allow-Methods": "GET, OPTIONS, POST",
                                "Access-Control-Allow-Headers": "*",
                            },
                            "body": json.dumps({"detail": error}),
                        }
        else:
            error = "Ya tiene la skin"
            return {
                "statusCode": 404,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": "*",
                    "Allow": "GET, OPTIONS, POST",
                    "Access-Control-Allow-Methods": "GET, OPTIONS, POST",
                    "Access-Control-Allow-Headers": "*",
                },
                "body": json.dumps({"detail": error}),
            }
