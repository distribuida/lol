import os
import pymysql


def lambda_handler(event, context):
    query_values = {}
    if (
        event["request"]["userAttributes"]["cognito:user_status"]
        == "EXTERNAL_PROVIDER"
    ):
        query_values = {
            "sub": event["request"]["userAttributes"]["sub"],
            "username": event["request"]["userAttributes"][
                "preferred_username"
            ],
        }
    else:
        query_values = {
            "sub": event["request"]["userAttributes"]["sub"],
            "username": event["userName"],
        }
    conn = pymysql.connect(
        host=os.environ["database_host"],
        user=os.environ["database_user"],
        passwd=os.environ["database_password"],
        db=os.environ["database_name"],
    )
    cur = conn.cursor()
    cur.execute(
        """INSERT INTO User (id, User_name, User_coins) VALUES (%(sub)s, %(username)s, 5000)""",
        query_values,
    )
    conn.commit()
    return event
