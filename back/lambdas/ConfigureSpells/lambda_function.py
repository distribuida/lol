import json
import os
import pymysql


def lambda_handler(event, context):
    # TODO implement

    conn = pymysql.connect(
        host=os.environ["database_host"],
        user=os.environ["database_user"],
        passwd=os.environ["database_password"],
        db=os.environ["database_name"],
    )

    cur = conn.cursor()
    error = ""

    if event["httpMethod"] == "GET":

        buildSpells = []
        spells = {}
        request = event["queryStringParameters"]

        if "user" in request:

            user = request["user"]
            index = []
            index.append("user")

            if not cur.execute("SELECT * FROM User where id = %s", str(user)):
                error = "No se ha encontrado el usuario asociado"
                return {
                    "statusCode": 404,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Origin": "*",
                        "Allow": "GET, OPTIONS, POST",
                        "Access-Control-Allow-Methods": "GET, OPTIONS, POST",
                        "Access-Control-Allow-Headers": "*",
                    },
                    "body": json.dumps({"detail": error}),
                }

            if not cur.execute(
                "SELECT * FROM User_Chossing_Spells where user = %s", str(user)
            ):
                error = "No se ha detectado que el usuario tenga alguna configuracion de hechizos"
                return {
                    "statusCode": 404,
                    "headers": {
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Origin": "*",
                        "Allow": "GET, OPTIONS, POST",
                        "Access-Control-Allow-Methods": "GET, OPTIONS, POST",
                        "Access-Control-Allow-Headers": "*",
                    },
                    "body": json.dumps({"detail": error}),
                }

            consulta = cur.fetchall()

            buildSpells.append(consulta[0][1])

            for spell in consulta:
                spells = {"id": spell[0], "spells": [spell[3], spell[4]]}
                buildSpells.append(spells)

                index.append(spell[2])

            spells = {i: info for i, info in zip(index, buildSpells)}

            return {
                "statusCode": 200,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": "*",
                    "Allow": "GET, OPTIONS, POST",
                    "Access-Control-Allow-Methods": "GET, OPTIONS, POST",
                    "Access-Control-Allow-Headers": "*",
                },
                "body": json.dumps(spells),
            }

        else:
            error = "No se ha detectado un usuario"
            return {
                "statusCode": 409,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": "*",
                    "Allow": "GET, OPTIONS, POST",
                    "Access-Control-Allow-Methods": "GET, OPTIONS, POST",
                    "Access-Control-Allow-Headers": "*",
                },
                "body": json.dumps({"detail": error}),
            }

    elif event["httpMethod"] == "POST":

        request_json = json.loads(event["body"])
        user = request_json["user"]
        champion = request_json["champion"]
        spell_principal = request_json["spell_principal"]
        spell_secondary = request_json["spell_secondary"]

        if not cur.execute("SELECT * FROM User where id = %s", str(user)):
            error = "No se ha encontrado el usuario asociado"
            return {
                "statusCode": 404,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": "*",
                    "Allow": "GET, OPTIONS, POST",
                    "Access-Control-Allow-Methods": "GET, OPTIONS, POST",
                    "Access-Control-Allow-Headers": "*",
                },
                "body": json.dumps({"detail": error}),
            }

        if spell_principal.__eq__(spell_secondary):
            error = "The user it is choosing the same Spell."
            return {
                "statusCode": 409,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": "*",
                    "Allow": "GET, OPTIONS, POST",
                    "Access-Control-Allow-Methods": "GET, OPTIONS, POST",
                    "Access-Control-Allow-Headers": "*",
                },
                "body": json.dumps({"detail": error}),
            }

        else:
            sql = "INSERT INTO User_Chossing_Spells (User, Champion, Spell_Principal, Spell_Secondary) VALUES (%s, %s, %s, %s)"
            cur.execute(
                sql, (user, champion, spell_principal, spell_secondary)
            )
            conn.commit()

            return {
                "statusCode": 201,
                "headers": {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": "*",
                    "Allow": "GET, OPTIONS, POST",
                    "Access-Control-Allow-Methods": "GET, OPTIONS, POST",
                    "Access-Control-Allow-Headers": "*",
                },
                "body": json.dumps(request_json),
            }
