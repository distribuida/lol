import json
import os
import pymysql


def lambda_handler(event, context):
    conn = pymysql.connect(
        host=os.environ["database_host"],
        user=os.environ["database_user"],
        passwd=os.environ["database_password"],
        db=os.environ["database_name"],
    )

    cur = conn.cursor()

    if event["httpMethod"] == "POST":
        request_json = json.loads(event["body"])
        user_sub = request_json["sub"]
        cur.execute(
            "SELECT User_name, User_coins, User_level FROM User WHERE id = %s",
            (user_sub),
        )
        query_result = cur.fetchall()[0]
        result = {
            "username": query_result[0],
            "coins": query_result[1],
            "level": query_result[2],
        }
        return {
            "statusCode": 200,
            "headers": {
                "Content-Type": "application/json",
                "Access-Control-Allow-Origin": "*",
                "Allow": "GET, OPTIONS, POST",
                "Access-Control-Allow-Methods": "GET, OPTIONS, POST",
                "Access-Control-Allow-Headers": "*",
            },
            "body": json.dumps(result),
        }
    else:
        return {
            "statusCode": 405,
            "headers": {
                "Content-Type": "application/json",
                "Access-Control-Allow-Origin": "*",
                "Allow": "GET, OPTIONS, POST",
                "Access-Control-Allow-Methods": "GET, OPTIONS, POST",
                "Access-Control-Allow-Headers": "*",
            },
        }
