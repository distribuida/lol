from cognito import (
    CognitoAuth,
    CognitoSettings,
    CognitoToken,
)
from dotenv import (
    load_dotenv,
)
from fastapi import (
    Depends,
    FastAPI,
    HTTPException,
)
import json
import os
from pydantic import (
    BaseModel,
    BaseSettings,
)
import pymysql
from typing import (
    List,
)

# Cargar las variables de entorno
load_dotenv()


class Settings(BaseSettings):
    check_expiration = True
    jwt_header_prefix = "Bearer"
    jwt_header_name = "Authorization"
    userpools = {
        "lol-user-pool": {
            "region": os.getenv("USERPOOL_REGION"),
            "userpool_id": os.getenv("USERPOOL_ID"),
            "app_client_id": os.getenv("APP_CLIENT_ID"),
        }
    }


settings = Settings()

cognito_lol_user_pool = CognitoAuth(
    settings=CognitoSettings.from_global_settings(settings),
    userpool_name="lol-user-pool",
)

# uvicorn main:app --reload
app = FastAPI()

conn = pymysql.connect(
    host=os.getenv("database_host"),
    user=os.getenv("database_user"),
    passwd=os.getenv("database_password"),
    db=os.getenv("database_name"),
)
cur = conn.cursor()

error = ""
buildSpells = []
spells = {}
index = []


@app.get("/riot/")
def read_item(user_id: str):

    if not cur.execute("SELECT * FROM User where id = %s", str(user_id)):
        error = "No se ha encontrado el usuario asociado"
        raise HTTPException(status_code=404, detail=error)

    cur.execute(
        "SELECT u.id, u.User_name, us.Champion_Skin, us.Skin_Price, us.is_Champion, u.User_level FROM User AS u JOIN User_Shopping_Skins AS us ON u.id = us.user WHERE u.id = %s",
        user_id,
    )
    sql = cur.fetchall()
    index = ["user_name", "user_level", "champion", "spent_coins"]
    champion = []
    price = 0
    values = []
    riot = {}
    values.append(sql[0][1])
    values.append(sql[0][5])

    for info in sql:
        if info[4] == 1:
            champion.append(info[2])
        price += info[3]

    values.append(champion)
    values.append(price)

    riot = {i: info for i, info in zip(index, values)}

    return riot


@app.get("/spells")
def read_spellg(
    user: str,
    auth: CognitoToken = Depends(cognito_lol_user_pool.auth_required),
):

    index.append("user")

    if not cur.execute("SELECT * FROM User where id = %s", str(user)):
        error = "No se ha encontrado el usuario asociado"
        raise HTTPException(status_code=404, detail=error)

    if not cur.execute(
        "SELECT * FROM User_Chossing_Spells where user = %s", str(user)
    ):
        error = "No se ha detectado que el usuario tenga alguna configuracion de hechizos"
        raise HTTPException(status_code=404, detail=error)

    consulta = cur.fetchall()

    buildSpells.append(consulta[0][1])

    for spell in consulta:
        spells = {"id": spell[0], "spells": [spell[3], spell[4]]}
        buildSpells.append(spells)

        index.append(spell[2])

    spells = {i: info for i, info in zip(index, buildSpells)}

    return spells


class Spell(BaseModel):
    user: str
    champion: str
    spell_principal: str
    spell_secondary: str


@app.post("/configure-spells", status_code=201)
def read_spellp(
    spell: Spell,
    auth: CognitoToken = Depends(cognito_lol_user_pool.auth_required),
):

    index.append("user")

    if not cur.execute("SELECT * FROM User where id = %s", str(Spell.user)):
        error = "No se ha encontrado el usuario asociado"
        raise HTTPException(status_code=404, detail=error)

    if Spell.spell_principal.__eq__(Spell.spell_secondary):

        raise HTTPException(status_code=404, detail=error)

    else:
        sql = "INSERT INTO User_Chossing_Spells (User, Champion, Spell_Principal, Spell_Secondary) VALUES (%s, %s, %s, %s)"
        cur.execute(
            sql,
            (
                Spell.user,
                Spell.champion,
                Spell.spell_principal,
                Spell.spell_secondary,
            ),
        )
        conn.commit()

        return spell


buildItems = []
items = {}


class UserChampion(BaseModel):
    user: str
    champion: str


@app.get("/items")
def read_item(
    user: str,
    champion: str,
    auth: CognitoToken = Depends(cognito_lol_user_pool.auth_required),
):

    if not cur.execute("SELECT * FROM User where id = %s", str(user)):
        error = "No se ha encontrado el usuario asociado"
        raise HTTPException(status_code=404, detail=error)

    index = []
    index.append("user")

    if not cur.execute(
        "SELECT * FROM User_Choosing_Items where user = %s and champion = %s",
        (str(user), str(champion)),
    ):
        error = "No se ha detectado que el usuario tenga alguna configuracion de items"
        raise HTTPException(status_code=404, detail=error)

    consulta = cur.fetchall()

    buildItems.append(consulta[0][1])

    for item in consulta:
        items = {"id": item[0], "items": [item[i] for i in range(3, 9)]}
        buildItems.append(items)

        index.append(item[2])

    items = {i: info for i, info in zip(index, buildItems)}

    return items


class Item(BaseModel):
    user: str
    champion: str
    items: List[str]


@app.post("/configure-items", status_code=201)
def read_item(
    item: Item,
    auth: CognitoToken = Depends(cognito_lol_user_pool.auth_required),
):

    if not cur.execute("SELECT * FROM User where id = %s", str(item.user)):
        error = "No se ha encontrado el usuario asociado"
        raise HTTPException(status_code=404, detail=error)

    if len(items) == 6:
        for i in range(0, len(items)):
            for j in range(i + 1, len(items)):
                error = "The user it is choosing the same Spell."
                if items[i].__eq__(items[j]):
                    return {error}

        sql = "INSERT INTO User_Choosing_Items (`User`, Champion, Item_1, Item_2,Item_3, Item_4, Item_5, Item_6) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"
        cur.execute(
            sql,
            (
                item.user,
                item.champion,
                item.items[0],
                item.items[1],
                item.items[2],
                item.items[3],
                item.items[4],
                item.items[5],
            ),
        )
        conn.commit()

        return item

    else:
        error = "No se ha enviado la cantidad correcta de parametros"
        raise HTTPException(status_code=404, detail=error)
