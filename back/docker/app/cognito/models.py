from pydantic import (
    BaseModel,
    Field,
    HttpUrl,
)
from typing import (
    Any,
    Dict,
    List,
    Set,
    Union,
)


class UserpoolModel(BaseModel):
    region: str
    userpool_id: str
    app_client_id: Union[str, List[str], Set[str], Dict[str, Any]]


class CognitoToken(BaseModel):
    cognito_id: str = Field(alias="sub")
    token_use: str
    auth_time: int
    iss: HttpUrl
    exp: int
    iat: int
    jti: str
    username: str = Field(alias="cognito:username")
