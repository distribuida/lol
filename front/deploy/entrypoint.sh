# shellcheck shell=bash

function main {
  local out='front/src/build'
  local bucket
  local env="${1}"
  local branch="${2:-default}"

  case "${env}" in
    prod)
      bucket='s3://lol-web.games/'
      ;;
    dev)
      bucket="s3://lol-web.games/${branch}/"
      ;;
    *) error 'Either "prod" or "dev" must be passed as arg' ;;
  esac \
    && aws s3 sync "${out}" "${bucket}" --delete
}

main "${@}"
