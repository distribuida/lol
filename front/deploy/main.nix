{
  inputs,
  makeScript,
  ...
}:
makeScript {
  name = "front-deploy";
  searchPaths = {
    bin = [
      inputs.nixpkgs.awscli
    ];
  };
  entrypoint = ./entrypoint.sh;
}
