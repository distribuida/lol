{
  secretsForTerraformFromEnv = {
    frontProd = {
      cloudflareAccountId = "CLOUDFLARE_ACCOUNT_ID";
      cloudflareApiToken = "CLOUDFLARE_API_TOKEN";
    };
  };
  secretsForAwsFromEnv = {
    frontProd = {
      accessKeyId = "AWS_ACCESS_KEY_ID";
      secretAccessKey = "AWS_SECRET_ACCESS_KEY";
    };
  };
  deployTerraform = {
    modules = {
      frontInfra = {
        src = "/front/infra/src";
        version = "1.0";
      };
    };
  };
}
