variable "CLOUDFLARE_ACCOUNT_ID" {
  type        = string
  description = "The Cloudflare Account ID"
}
variable "CLOUDFLARE_API_TOKEN" {
  type        = string
  description = "The Cloudflare API token"
}
variable "aws_region" {
  type        = string
  description = "The AWS region to put the bucket into"
  default     = "us-east-2"
}
variable "site_domain" {
  type        = string
  description = "The domain name to use for the static site"
  default     = "lol-web.games"
}
