module.exports = {
    parserOpts: {
      headerPattern: /^(lol)\\(\w*)\((\w*)\):\s(#[1-9]\d*)(.\d+)?\s(.*)$/,
      headerCorrespondence: [ 'product', 'type', 'scope', 'ticket', 'part', 'subject' ],
    },
  };
  