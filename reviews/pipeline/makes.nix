{gitlabCi, ...}: let
  gitlabBranchNotMain = gitlabCi.rules.branchNot "main";

  gitlabTitleMatchingReviews = gitlabCi.rules.titleMatching "^(all|reviews)";

  gitlabOnlyDev = [
    gitlabBranchNotMain
    gitlabCi.rules.notMrs
    gitlabCi.rules.notSchedules
    gitlabCi.rules.notTriggers
    gitlabTitleMatchingReviews
  ];

  gitlabLint = {
    rules = gitlabOnlyDev;
    stage = "lint-code";
  };
in {
  pipelines = {
    reviews = {
      gitlabPath = "/reviews/gitlab-ci.yaml";
      jobs = [
        {
          output = "/lintPython/dirOfModules/reviews";
          gitlabExtra = gitlabLint;
        }
      ];
    };
  };
}
