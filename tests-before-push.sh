#!/bin/bash
# shellcheck shell=bash

message=""
success=false
RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'

if m . /formatBash; then
  success=true
  message+="${GREEN}/formatBash job succeeded\n${NC}"
else
  success=false
  message+="${RED}/formatBash job failed\n${NC}"
fi

if m . /formatMarkdown; then
  success=true
  message+="${GREEN}/formatMarkdown job succeeded\n${NC}"
else
  success=false
  message+="${RED}/formatMarkdown job failed\n${NC}"
fi

if m . /formatNix; then
  success=true
  message+="${GREEN}/formatNix job succeeded\n${NC}"
else
  success=false
  message+="${RED}/formatNix job failed\n${NC}"
fi

if m . /formatPython; then
  success=true
  message+="${GREEN}/formatPython job succeeded\n${NC}"
else
  success=false
  message+="${RED}/formatPython job failed\n${NC}"
fi

if m . /formatTerraform; then
  success=true
  message+="${GREEN}/formatTerraform job succeeded\n${NC}"
else
  success=false
  message+="${RED}/formatTerraform job failed\n${NC}"
fi

if m . /formatYaml; then
  success=true
  message+="${GREEN}/formatYaml job succeeded\n${NC}"
else
  success=false
  message+="${RED}/formatYaml job failed\n${NC}"
fi

if m . /lintBash; then
  success=true
  message+="${GREEN}/lintBash job succeeded\n${NC}"
else
  success=false
  message+="${RED}/lintBash job failed\n${NC}"
fi

if m . /lintGitCommitMsg; then
  success=true
  message+="${GREEN}/lintGitCommitMsg job succeeded\n${NC}"
else
  success=false
  message+="${RED}/lintGitCommitMsg job failed\n${NC}"
fi

if m . /lintGitMailMap; then
  success=true
  message+="${GREEN}/lintGitMailMap job succeeded\n${NC}"
else
  success=false
  message+="${RED}/lintGitMailMap job failed\n${NC}"
fi

if m . /lintNix; then
  success=true
  message+="${GREEN}/lintNix job succeeded\n${NC}"
else
  success=false
  message+="${RED}/lintNix job failed\n${NC}"
fi

echo -e "$message"

if $success; then

  echo -e "${GREEN}All jobs succeded, you can push to the repo${NC}"
else
  echo -e "${RED}Not all jobs succeded, undo the last commit, resolve the issues and re-run this test${NC}"
fi
