CREATE DATABASE `lol-database`;

use `lol-database`;

CREATE TABLE `User` (
  `User_id` int(11) NOT NULL,
  `User_name` varchar(128) NOT NULL,
  `User_coins` int(20) NOT NULL,
  PRIMARY KEY (`User_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `Champion` (
  `Champion_id` int(11) NOT NULL,
  `User_id` int(11) NOT NULL,
   PRIMARY KEY (`Champion_id`),
   FOREIGN KEY (`User_id`) references User(User_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `Skin` (
  `Skin_id` int(11) NOT NULL,
  `Champion_id` int(11) NOT NULL,
  PRIMARY KEY (`Skin_id`),
  FOREIGN KEY (`Champion_id`) references Champion(Champion_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `Rune` (
  `Rune_id` int(11) NOT NULL,
  `Rune_name` varchar(128) NOT NULL,
  `Rune_description` varchar(128) NOT NULL,
  PRIMARY KEY (`Rune_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `RuneClave` (
  `RuneClave_id` int(11) NOT NULL,
  `RuneClave_name` varchar(128) NOT NULL,
  `RuneClave_description` varchar(128) NOT NULL,
  `Rune_id` int(11) NOT NULL,
  PRIMARY KEY (`RuneClave_id`),
  FOREIGN KEY (`Rune_id`) references Rune(Rune_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `RuneSlot` (
  `RuneSlot_id` int(11) NOT NULL,
  `RuneSlot_name` varchar(128) NOT NULL,
  `RuneSlot_description` varchar(128) NOT NULL,
  `RuneSlot_type` int(11) NOT NULL,
  `Rune_id` int(11) NOT NULL,
  PRIMARY KEY (`RuneSlot_id`),
  FOREIGN KEY (`Rune_id`) references Rune(Rune_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `RuneAdaptative` (
  `RuneAdaptative_id` int(11) NOT NULL,
  `RuneAdaptative_name` varchar(128) NOT NULL,
  `RuneAdaptative_description` varchar(128) NOT NULL,
  `RuneAdaptative_type` int(11) NOT NULL,
  PRIMARY KEY (`RuneAdaptative_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `Item` (
  `Item_id` int(11) NOT NULL,
  `Item_name` varchar(128) NOT NULL,
  `Item_description` varchar(128) NOT NULL,
  `Item_price` int(20) NOT NULL,
  PRIMARY KEY (`Item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `BuildRune` (
  `BuildRune_id` int(11) NOT NULL,
  `FirstRune_id` int(11) NOT NULL,
  `FirstRuneClave_id` int(11) NOT NULL,
  `FirstRuneSlotFirst_id` int(11) NOT NULL,
  `FirstRuneSlotSecondType_id` int(11) NOT NULL,
  `FirstRuneSlotTirdType_id` int(11) NOT NULL,
  `SecondRune_id` int(11) NOT NULL,
  `SecondRuneSlotFirst_id` int(11) NOT NULL,
  `SecondRuneSlotSecondType_id` int(11) NOT NULL,
  `RuneOffense_id` int(11) NOT NULL,
  `RuneFlex_id` int(11) NOT NULL,
  `RuneDefence_id` int(11) NOT NULL,
  `User_id` int(11) NOT NULL,
  PRIMARY KEY (`BuildRune_id`),
  FOREIGN KEY (`FirstRune_id`) references Rune(Rune_id),
  FOREIGN KEY (`FirstRuneClave_id`) references RuneClave(RuneClave_id),
  FOREIGN KEY (`FirstRuneSlotFirst_id`) references RuneSlot(RuneSlot_id),
  FOREIGN KEY (`FirstRuneSlotSecondType_id`) references RuneSlot(RuneSlot_id),
  FOREIGN KEY (`FirstRuneSlotTirdType_id`) references RuneSlot(RuneSlot_id),
  FOREIGN KEY (`SecondRune_id`) references Rune(Rune_id),
  FOREIGN KEY (`SecondRuneSlotFirst_id`) references RuneSlot(RuneSlot_id),
  FOREIGN KEY (`SecondRuneSlotSecondType_id`) references RuneSlot(RuneSlot_id),
  FOREIGN KEY (`RuneOffense_id`) references RuneAdaptative(RuneAdaptative_id),
  FOREIGN KEY (`RuneFlex_id`) references RuneAdaptative(RuneAdaptative_id),
  FOREIGN KEY (`RuneDefence_id`) references RuneAdaptative(RuneAdaptative_id),
  FOREIGN KEY (`User_id`) references User(User_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `BuildItem` (
  `BuildItem_id` int(11) NOT NULL,
  `Item_uno` int(11) NOT NULL,
  `Item_dos` int(11) NOT NULL,
  `Item_tres` int(11) NOT NULL,
  `Item_cuatro` int(11) NOT NULL,
  `Item_cinco` int(11) NOT NULL,
  `Item_seis` int(11) NOT NULL,
  `User_id` int(11) NOT NULL,
  PRIMARY KEY (`BuildItem_id`),
  FOREIGN KEY (`Item_uno`) references Item(Item_id),
  FOREIGN KEY (`Item_dos`) references Item(Item_id),
  FOREIGN KEY (`Item_tres`) references Item(Item_id),
  FOREIGN KEY (`Item_cuatro`) references Item(Item_id),
  FOREIGN KEY (`Item_cinco`) references Item(Item_id),
  FOREIGN KEY (`Item_seis`) references Item(Item_id),
  FOREIGN KEY (`User_id`) references User(User_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
